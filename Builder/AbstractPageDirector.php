<?php

/**
 * Created by PhpStorm.
 * User: ady
 * Date: 28.03.2016
 * Time: 10:42
 */
abstract class AbstractPageDirector
{
    abstract function __construct(AbstractPageBuilder $builder_in);

    abstract function buildPage();

    abstract function getPage();
}