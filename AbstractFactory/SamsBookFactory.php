<?php
/**
 * Created by PhpStorm.
 * User: ady
 * Date: 23.03.2016
 * Time: 21:23
 */
require_once('AbstractBookFactory.php');
require_once('SamsPHPBook.php');
require_once('SamsMySQLBook.php');

class SamsBookFactory extends AbstractBookFactory {

    function makePHPBook()
    {
        return new SamsPHPBook;
    }

    function makeMySQLBook()
    {
        return new SamsMySQLBook;
    }
}