<?php
/**
 * Created by PhpStorm.
 * User: ady
 * Date: 23.03.2016
 * Time: 21:27
 */
include_once('AbstractMySQLBook.php');

class SamsMySQLBook extends AbstractMySQLBook{
    private $author;

    private $title;

    function __construct() {

        $this->author = 'Paul Dubois';
        $this->title  = 'MySQL, 3rd Edition';

    }

    function getAuthor() {return $this->author;}

    function getTitle() {return $this->title;}
}